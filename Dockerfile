# Stage 1: Build the React app
FROM node:14-alpine AS react-build
WORKDIR /app
COPY frontend/package.json frontend/package-lock.json ./
RUN npm ci --silent
COPY frontend/public ./public
COPY frontend/src ./src
RUN npm run build

# Stage 2: Build the Python FastAPI backend
FROM python:3.9 AS backend-build
WORKDIR /app
COPY backend/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY backend/backend ./backend

# todo: insert here actual backend build logic

# Stage 3: Combine the React app and the Python FastAPI backend with Nginx
FROM nginx:1.21-alpine
COPY --from=react-build /app/build /usr/share/nginx/html
COPY --from=backend-build /app/backend /app/backend
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8000
CMD ["nginx", "-g", "daemon off;"]
