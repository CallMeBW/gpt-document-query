import React, { useState } from 'react';
import axios, { CancelTokenSource } from 'axios';
import './App.css';
import circle from './circle.svg'

interface RequestData {
  file: File;
}

const App: React.FC = () => {
  const [file, setFile] = useState<File | null>(null);
  const [question, setQuestoin] = useState('What is the total price?');
  const [response, setResponse] = useState('');
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [cancelToken, setCancelToken] = useState<CancelTokenSource | null>(null);

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      setFile(event.target.files[0]);
    }
  };

  const handleQueryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuestoin(event.target.value);
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!file || !question || loading) {
      return;
    }

    setResponse("")
    setIsError(false);
    setLoading(true);

    const formData = new FormData();
    formData.append('file', file);

    try {
      const cancelTokenSource = axios.CancelToken.source();
      setCancelToken(cancelTokenSource);

      const response = await axios.post(
        `http://127.0.0.1:8000/process_request_file?question=${question}`,
        formData,
        { cancelToken: cancelTokenSource.token }
      );

      setResponse(response.data.answer);
      // Process the response as needed
    } catch (error) {
      if (axios.isCancel(error)) {
        console.log('Request canceled:', error.message);
      } else {
        setIsError(true);
        console.error(error);
      }
    } finally {
      setLoading(false);
      setCancelToken(null);
    }
  };

  const handleCancel = () => {
    if (cancelToken) {
      cancelToken.cancel('Request canceled by user.');
    }
  };

  return (
    <div className="container">
      <h1>Document Wizard</h1>
      <form onSubmit={handleSubmit}>
        <input
          className="file-input"
          type="file"
          accept=".pdf"
          onChange={handleFileChange}
          disabled={loading}
        />
        <div className="horizontal-row">
          <input
            className="query-input"
            type="text"
            value={question}
            onChange={handleQueryChange}
            placeholder="Ask anything about the document"
            disabled={loading}
          />
          <button
            className="submit-button" type="submit"
            disabled={loading || !file || !question}>
            Submit
          </button>

        </div>
        {loading &&
          <>
            <img className="loading-circle" src={circle} alt="Icon" />
            <button className="cancel-button" type="button" onClick={handleCancel}>
              Cancel
            </button>
          </>
        }
      </form>
      {response &&
        <div className="response">
          <div className="response-title">Answer:</div>
          <div >{response}</div>
        </div>
      }

      {isError &&
        <div className="response error">
          <div >An error occurred. Please try again.</div>
        </div>
      }

    </div>
  );
};

export default App;
