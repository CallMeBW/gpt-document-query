# Document Wizard

Document Wizard is a web application that allows users to upload PDF documents and ask queries related to the content of the document. The application utilizes a sentence encoder and the OpenAI API Chat Completion endpoints, to provide answers to user queries.


## Credit

This application is inspired by the work of [bhaskatripathi](https://github.com/bhaskatripathi) and the [pdfGPT](https://github.com/bhaskatripathi/pdfGPT) project. The original project provided a foundation for integrating OpenAI API with PDF processing capabilities, which has been further expanded upon to create Document Wizard.

## Usage

To run the application, follow these steps:

1. Make sure you have [Docker](https://www.docker.com/) installed and running on your system.
2. Download the universal sentence encoder from [here](https://tfhub.dev/google/universal-sentence-encoder/4?tf-hub-format=compressed) and place the unpacked directory in the root directory as `universal-sentence-encoder`
3. Open the terminal and run the following command: `docker-compose up`
4. Once the application is up and running, open your web browser and go to `localhost:3000`.
5. You should now see the Document Wizard interface where you can upload PDF files and ask queries.
