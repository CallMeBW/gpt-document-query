import urllib.request
import fitz
import re
import openai
import os
from fastapi import FastAPI, UploadFile, HTTPException
from fastapi.middleware.cors import CORSMiddleware


from .semantic_search import SemanticSearch

OPENAI_KEY = os.environ['OPENAI_KEY']

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def download_pdf(url, output_path):
    urllib.request.urlretrieve(url, output_path)

def preprocess(text):
    text = text.replace('\n', ' ')
    text = re.sub('\s+', ' ', text)
    return text

def pdf_to_text(path: str, start_page=1):
    doc = fitz.open(path)
    text_list = []

    for i in range(start_page-1, doc.page_count):
        text = doc.load_page(i).get_text("text")
        text = preprocess(text)
        text_list.append(text)

    doc.close()
    return text_list

def text_to_chunks(texts, word_length=150, start_page=1):
    text_toks = [t.split(' ') for t in texts]
    chunks = []

    for idx, words in enumerate(text_toks):
        for i in range(0, len(words), word_length):
            chunk = words[i:i+word_length]
            if (i+word_length) > len(words) and (len(chunk) < word_length) and (
                len(text_toks) != (idx+1)):
                text_toks[idx+1] = chunk + text_toks[idx+1]
                continue
            chunk = ' '.join(chunk).strip()
            chunk = f'[Page no. {idx+start_page}]' + ' ' + '"' + chunk + '"'
            chunks.append(chunk)
    return chunks

def load_recommender(path, start_page=1):
    global semantic_search
    texts = pdf_to_text(path, start_page=start_page)
    chunks = text_to_chunks(texts, start_page=start_page)
    semantic_search.fit(chunks)

def generate_text(prompt, engine):
    openai.api_key = OPENAI_KEY
    completions = openai.ChatCompletion.create(
        model=engine,
        n=1,
        stop=None,
        temperature=0.7,
        messages=[{"role": "user", "content": prompt}]
    )
    message = completions.choices[0].message.content
    return message

def generate_answer(question):
    topn_chunks = semantic_search.get_matches(question)
    prompt = ""
    prompt += 'search results:\n\n'
    for c in topn_chunks:
        prompt += c + '\n\n'

    prompt += "Instructions: Compose a comprehensive reply to the query using the search results given. "\
              "Cite each reference using [ Page Number] notation (every result has this number at the beginning). "\
              "Citation should be done at the end of each sentence. If the search results mention multiple subjects "\
              "with the same name, create separate answers for each. Only include information found in the results and "\
              "don't add any additional information. Make sure the answer is correct and don't output false content. "\
              "If the text does not relate to the query, simply state 'Found Nothing'. Ignore outlier "\
              "search results which have nothing to do with the question. Only answer what is asked. The "\
              "answer should be short and concise. \n\n"

    prompt += f"Query: {question}\nAnswer:"
    print(prompt)
    # answer = generate_text(prompt, "text-davinci-003")
    answer = generate_text(prompt, "gpt-3.5-turbo")
    return answer

semantic_search = SemanticSearch()


@app.post("/process_request_url")
def process_request_url(url: str, question: str):
    if not url.strip() or not question.strip():
        raise HTTPException(status_code=400, detail="URL and question are required.")

    file_name = '/tmp/text.pdf'
    download_pdf(url, file_name)
    load_recommender(file_name)
    return {"answer": generate_answer(question)}


@app.post("/process_request_file")
def process_request_file(file: UploadFile, question: str):
    if not question.strip():
        raise HTTPException(status_code=400, detail="Question is required.")

    file_name = '/tmp/text.pdf'
    with open(file_name, "wb") as f:
        f.write(file.file.read())

    load_recommender(file_name)
    return {"answer": generate_answer(question)}
